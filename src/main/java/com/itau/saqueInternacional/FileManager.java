package com.itau.saqueInternacional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileManager {


	public static ArrayList<String> readfile(String path) {


		try {
			return (ArrayList<String>) Files.readAllLines(Paths.get(path));

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}	
	}


	public static void writefile(String content, String path){


		try {
			Files.write(Paths.get(path), content.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}